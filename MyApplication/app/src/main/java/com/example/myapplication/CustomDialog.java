package com.example.myapplication;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class CustomDialog extends Dialog implements View.OnClickListener {

    private TextView titleTv,detailTv;
    private Button cancelBtn,confirmBtn;
    private OnCancelInterface onCancel;
    private OnConfirmInterface onConfirm;

    public CustomDialog setTitle(String title) {
        this.title = title;
        return this;

    }

    public CustomDialog setMessage(String message) {
        this.message = message;
        return this;

    }

    public CustomDialog setCancel(String cancel, OnCancelInterface onCancel) {
        this.cancel = cancel;
        this.onCancel = onCancel;
        return this;
    }

    public CustomDialog setConfirm(String confirm, OnConfirmInterface onConfirm) {
        this.confirm = confirm;
        this.onConfirm = onConfirm;
        return this;

    }

    String title,message,cancel,confirm;

    public CustomDialog(@NonNull Context context) {
        super(context);
    }

    public CustomDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected CustomDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_dialog_layout);

        //按空白处不能取消动画
        setCanceledOnTouchOutside(false);

        titleTv = findViewById(R.id.title_tv);
        detailTv = findViewById(R.id.detail_tv);
        cancelBtn = findViewById(R.id.cancel_btn);
        confirmBtn = findViewById(R.id.confirm_btn);

        cancelBtn.setOnClickListener(this);
        confirmBtn.setOnClickListener(this);

        if (!title.isEmpty()) {
            titleTv.setText(title);
        }
        if (!message.isEmpty()) {
            detailTv.setText(message);

        }
        if (!cancel.isEmpty()) {
            detailTv.setText(message);

        }
        if (!confirm.isEmpty()) {
            confirmBtn.setText(confirm);
        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancel_btn: {
                onCancel.cancel();
                dismiss();
            } break;
            case  R.id.confirm_btn: {
                onConfirm.confirm();
                dismiss();
            } break;
        }
    }


    public interface OnCancelInterface {
        public void cancel();
    }
    public interface OnConfirmInterface {
        public void  confirm();
    }
}
