package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;

public class addViewActivity extends AppCompatActivity {

    private ConstraintLayout layout;
    private LinearLayout linearLayout;

    private String name;
    private int age;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_view);
        layout = findViewById(R.id.root_view);
        linearLayout = findViewById(R.id.container);

        name = getIntent().getStringExtra("name");
        age = getIntent().getIntExtra("age", 0);

        Toast.makeText(addViewActivity.this, "名称" + name + "年龄" + String.valueOf(age), Toast.LENGTH_SHORT).show();

        findViewById(R.id.button1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addView();
            }
        });
        findViewById(R.id.button2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addviewTwo();
            }
        });
    }
    /**
     * 按钮一的点击事件
     */
    private void addView() {
        TextView textView = new TextView(this);
        textView.setTextSize(25);
        //获取当前时间并格式化
        String currentTime = dateToStamp(System.currentTimeMillis());
        textView.setText("测试一...测试一...测试一...测试一...测试一...测试一...测试一...测试一...测试一...测试一...测试一...测试一..."+currentTime);
        textView.setTextColor(getResources().getColor(R.color.purple_500));

        linearLayout.addView(textView,0);
    }
    /**
     * 按钮二的点击事件
     */
    private void addviewTwo() {
        TextView textView = new TextView(this);
        //获取当前时间并格式化
        String currentTime = dateToStamp(System.currentTimeMillis());
        textView.setText("测试二..."+currentTime);
        textView.setTextSize(20f);
        textView.setTextColor(getResources().getColor(R.color.purple_200));

        linearLayout.addView(textView,1);
    }
    /**
     *格式化事件
     */
    public String dateToStamp(long s) {
        String res;
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date(s);
            res = simpleDateFormat.format(date);
        } catch (Exception e) {
            return "";
        }
        return res;
    }

}