package com.example.myapplication;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.myapplication.utils.KeyboardUtils;
import com.wuhenzhizao.titlebar.widget.CommonTitleBar;

public class LoginActivity extends AppCompatActivity {
    private RadioGroup group;
    private RadioButton female, male;

    private Button alert1,alert2,alert3,alert4;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final CommonTitleBar titleBar = (CommonTitleBar) findViewById(R.id.title_view);
//        titleBar.setBackgroundResource(R.drawable.ic_launcher_background);
        titleBar.setListener(new CommonTitleBar.OnTitleBarListener() {

            @Override
            public void onClicked(View v, int action, String extra) {
                if (action == CommonTitleBar.ACTION_LEFT_TEXT ||
                        action == CommonTitleBar.ACTION_LEFT_BUTTON) {
                    onBackPressed();
                }
            }
        });

        group = findViewById(R.id.btn_group);
        female = findViewById(R.id.female_radio_btn);
        male = findViewById(R.id.male_radio_btn);

        alert1 = findViewById(R.id.alert_1);
        alert2 = findViewById(R.id.alert_2);
        alert3 = findViewById(R.id.alert_3);
        alert4 = findViewById(R.id.alert_4);

        OnClick click = new OnClick();
        alert1.setOnClickListener(click);
        alert2.setOnClickListener(click);
        alert3.setOnClickListener(click);
        alert4.setOnClickListener(click);


        Intent intent = getIntent();
        String name = intent.getStringExtra("name" );
        EditText userEdit = findViewById(R.id.username_edit);
        userEdit.setText(name);


        group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                RadioButton btn = findViewById(i);
                Toast.makeText(LoginActivity.this, btn.getText(), Toast.LENGTH_SHORT).show();
            }
        });



    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        //获取当前获得焦点的View
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            View view = getCurrentFocus();
            //调用方法判断是否需要隐藏键盘
            KeyboardUtils.hideKeyboard(ev, view, this);
        }
        return super.dispatchTouchEvent(ev);
    }


    public class OnClick implements View.OnClickListener {

        int checkedItem = 0;

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.alert_1: {
                    AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                    builder.setTitle("提示").setMessage("你确定删除吗？").setPositiveButton("ok", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    }).setNegativeButton("取消", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    }).show();
                }

                    break;
                case R.id.alert_2: {
                    /*
                     *  使用说明，AlertDialog 设置单选或者多选的的时候，不能设置message，否则单选和多选出不来
                     * */
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(LoginActivity.this);
                    String[] items = {"男", "女"};
                    builder1.setTitle("提示").setSingleChoiceItems(items, 0, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            checkedItem = which;

                        }
                    }).setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    }).show();

                }


                    break;

                case R.id.alert_3:{
                    AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                    String[] items = {"苹果", "香蕉"};
                    boolean[] checked = {true,false};
                    builder.setTitle("提示").setMultiChoiceItems(items, checked, new DialogInterface.OnMultiChoiceClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which, boolean isChecked) {

                        }
                    }).setPositiveButton("确定", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    }).show();
                }
                    break;
                case R.id.alert_4: {
                    // 自定义alert
                    AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                    View view = LayoutInflater.from(LoginActivity.this).inflate(R.layout.layout_alert, null);

                    builder.setView(view);


                    builder.show();
                }


                    break;
                default:
                    break;
            }
        }
    }


    @Override
    public void finish() {
        Intent intent = new Intent();
        intent.putExtra("age", 18);
        setResult(100, intent);  //resultCode==3

        super.finish();


    }
}