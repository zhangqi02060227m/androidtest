package com.example.myapplication;


public class UserModel {


    private DataDTO data;
    private String code;
    private String message;

    public DataDTO getData() {
        return data;
    }

    public void setData(DataDTO data) {
        this.data = data;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class DataDTO {
        private Integer total;
        private Integer pageNum;
        private Integer pageSize;
        private Integer size;
        private Integer startRow;
        private Integer endRow;
        private Integer pages;
        private Integer prePage;
        private Integer nextPage;
        private Boolean isFirstPage;
        private Boolean isLastPage;
        private Boolean hasPreviousPage;
        private Boolean hasNextPage;
        private Integer navigatePages;
        private Integer navigateFirstPage;
        private Integer navigateLastPage;

        public Integer getTotal() {
            return total;
        }

        public void setTotal(Integer total) {
            this.total = total;
        }

        public Integer getPageNum() {
            return pageNum;
        }

        public void setPageNum(Integer pageNum) {
            this.pageNum = pageNum;
        }

        public Integer getPageSize() {
            return pageSize;
        }

        public void setPageSize(Integer pageSize) {
            this.pageSize = pageSize;
        }

        public Integer getSize() {
            return size;
        }

        public void setSize(Integer size) {
            this.size = size;
        }

        public Integer getStartRow() {
            return startRow;
        }

        public void setStartRow(Integer startRow) {
            this.startRow = startRow;
        }

        public Integer getEndRow() {
            return endRow;
        }

        public void setEndRow(Integer endRow) {
            this.endRow = endRow;
        }

        public Integer getPages() {
            return pages;
        }

        public void setPages(Integer pages) {
            this.pages = pages;
        }

        public Integer getPrePage() {
            return prePage;
        }

        public void setPrePage(Integer prePage) {
            this.prePage = prePage;
        }

        public Integer getNextPage() {
            return nextPage;
        }

        public void setNextPage(Integer nextPage) {
            this.nextPage = nextPage;
        }

        public Boolean getIsFirstPage() {
            return isFirstPage;
        }

        public void setIsFirstPage(Boolean isFirstPage) {
            this.isFirstPage = isFirstPage;
        }

        public Boolean getIsLastPage() {
            return isLastPage;
        }

        public void setIsLastPage(Boolean isLastPage) {
            this.isLastPage = isLastPage;
        }

        public Boolean getHasPreviousPage() {
            return hasPreviousPage;
        }

        public void setHasPreviousPage(Boolean hasPreviousPage) {
            this.hasPreviousPage = hasPreviousPage;
        }

        public Boolean getHasNextPage() {
            return hasNextPage;
        }

        public void setHasNextPage(Boolean hasNextPage) {
            this.hasNextPage = hasNextPage;
        }

        public Integer getNavigatePages() {
            return navigatePages;
        }

        public void setNavigatePages(Integer navigatePages) {
            this.navigatePages = navigatePages;
        }

        public Integer getNavigateFirstPage() {
            return navigateFirstPage;
        }

        public void setNavigateFirstPage(Integer navigateFirstPage) {
            this.navigateFirstPage = navigateFirstPage;
        }

        public Integer getNavigateLastPage() {
            return navigateLastPage;
        }

        public void setNavigateLastPage(Integer navigateLastPage) {
            this.navigateLastPage = navigateLastPage;
        }
    }
}
