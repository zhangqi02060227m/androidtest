package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class NaviActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navi);

        FragmentManager fragmentManager=getSupportFragmentManager();
        List<Fragment> fragments=new ArrayList<Fragment>();
        fragments.add(new NaviHotFragment());
        fragments.add(new NaviSecondFragment());
        List<String> tabs=new ArrayList<>();
        tabs.add("首页");
        tabs.add("社区");
        NaviFragmentAdapter fragmentAdapter=new
                NaviFragmentAdapter(fragmentManager,fragments,tabs);

        ViewPager viewPager=(ViewPager)findViewById(R.id.view_pager);
        viewPager.setAdapter(fragmentAdapter);
        TabLayout tabLayout= (TabLayout) findViewById(R.id.tab_main);
        tabLayout.setupWithViewPager(viewPager);


    }
}