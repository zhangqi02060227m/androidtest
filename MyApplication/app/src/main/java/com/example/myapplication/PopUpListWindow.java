package com.example.myapplication;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class PopUpListWindow extends PopupWindow {

    private Activity context;
    String title;
    ArrayList <String> list = new ArrayList<>();
    private Button sureBtn;
    private ClickSureInferFace clickSureInferFace;

    public void setClickSureInferFace(ClickSureInferFace clickSureInferFace) {
        this.clickSureInferFace = clickSureInferFace;
    }


    RecyclerView recyclerView;
    TextView textView;
    PopUpAdapter adapter;

    public PopUpListWindow(Activity content,String title, ArrayList<String> list) {
        this.context = content;
        this.title = title;
        this.list = list;

        LayoutInflater inflater = (LayoutInflater) content.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View popView = inflater.inflate(R.layout.layout_popupwindow_list, null, false);


        setContentView(popView);
        sureBtn = popView.findViewById(R.id.sure_1);

        backgroundAlpha(0.5f);
        // 添加pop窗口关闭事件
        this.setOnDismissListener(new popOnDismissListener());

        RecyclerView recyclerView = popView.findViewById(R.id.recycle_1);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        PopUpAdapter adapter = new PopUpAdapter(content, list);
        this.adapter = adapter;
        recyclerView.setAdapter(adapter);


        sureBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clickSureInferFace != null) {
                    dismiss();
                    clickSureInferFace.clickOK();
                }
            }
        });
    }

    /**
     * PopouWindow设置添加屏幕的背景透明度
     * @param bgAlpha
     */
    public void backgroundAlpha(float bgAlpha) {
        WindowManager.LayoutParams lp = context.getWindow().getAttributes();
        lp.alpha = bgAlpha;
        context.getWindow().setAttributes(lp);
    }

    /**
     * PopouWindow关闭的事件，主要是为了将背景透明度改回来
     *
     * @author cg
     */
    class popOnDismissListener implements OnDismissListener {

        @Override
        public void onDismiss() {
            backgroundAlpha(1f);
        }
    }

    public interface ClickSureInferFace {
        void clickOK();
    }


}
