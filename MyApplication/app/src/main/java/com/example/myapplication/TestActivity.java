package com.example.myapplication;

import android.nfc.Tag;
import android.os.Bundle;

import com.example.myapplication.utils.ToastUtils;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.myapplication.databinding.ActivityTestBinding;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;

public class TestActivity extends AppCompatActivity {

    private AppBarConfiguration appBarConfiguration;
    private ActivityTestBinding binding;
    private  static  int PORT = 6666;
    private static String IP = "127.0.0.1";
    private boolean isRunning = true;
    private static final String TAG = "SocketConnectActivity";
    private DatagramSocket receiveSocket = null;
    private DatagramSocket sendSocket = null;
    private DatagramPacket dpReceive = null;
    private  String previousContent;
    private String receiveIp;
    private String sendIp;
    private Button testBtn;
    private Handler mHandler;
    private String RECV_MSG_KEY = "RECV_MSG_KEY";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityTestBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        testBtn = findViewById(R.id.testBtn);
        startListen();
        try {
            String add = getPhoneBroadcast();
            Log.i(TAG,add);
        } catch (SocketException e) {
            e.printStackTrace();
        }

        testBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG,"click");
                sendUDPData();
            }
        });

//        ReceiveThread receiveThread = new ReceiveThread();
//        try {
//            receiveSocket = new DatagramSocket(PORT);
//            sendThread = new SendThread();
//            sendThread.start();
//            receiveThread.start();
//        } catch (SocketException e) {
//            e.printStackTrace();
//        }

        mHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                //根据信息编码及数据做出相对应的处理
                switch (msg.what) {
                    case 1:
                        //更新 TextView UI
                        break;
                    case 2:
                        //获取 ProgressBar 的进度，然后显示进度值
                        Bundle bundle = msg.getData();
                        String content = bundle.getString(RECV_MSG_KEY);
                        ToastUtils.show(TestActivity.this,content);
                        break;
                    default:
                        break;
                }
            }
        };

    }

    public static void send(String message) {
//        ToastUtils.show(TestActivity.this, "我来了");

        int local_port=6666;//本机端口号
        int dest_port= 7000;//目标接收端口号
        DatagramSocket s = null;
        try {
            s = new DatagramSocket(local_port);
        } catch (SocketException e) {
            e.printStackTrace();
//            ToastUtils.show(TestActivity.this, "socket创建失败");
        }
        InetAddress local = null;
        try {
            local = InetAddress.getByName("127.0.0.1");

        } catch (UnknownHostException e) {
            e.printStackTrace();
//            ToastUtils.show(TestActivity.this, "ip地址创建失败");

        }
        int msg_length = message.length();
        byte[] messageByte = message.getBytes();
        DatagramPacket p = new DatagramPacket(messageByte, msg_length, local, dest_port);
        try {
            s.send(p);
        } catch (IOException e) {
            e.printStackTrace();
//            ToastUtils.show(TestActivity.this, "异常了");

        }
    }

    private void sendUDPData()
    {
        new Thread(new Runnable()
        {
            public void run() {
                try {
                    DatagramSocket socket = new DatagramSocket();
                    byte[] sendStr = "39.6666,116.355".getBytes();
                    InetAddress address = InetAddress.getByName("192.168.102.20");
                    DatagramPacket packet = new DatagramPacket(sendStr, sendStr.length, address, 7000);
                    socket.send(packet);
                    socket.close();
                } catch (SocketException e) {
                    e.printStackTrace();
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void startListen() {
        new Thread(new Runnable() {
            public void run() {
                DatagramSocket receviceSocket = null;
                try {
                    byte[] bytedata = new byte[1024];
                    receviceSocket = new DatagramSocket(6666);
                    DatagramPacket recevicePackt = new DatagramPacket(bytedata, bytedata.length);
//                    InetAddress address = InetAddress.getByName("192.168.102.255");
                    DatagramPacket ackPacket = new DatagramPacket(bytedata, bytedata.length);

                    while (true)
                    {
                        receviceSocket.receive(recevicePackt);
                        String receviceStr = new String(bytedata, 0, recevicePackt.getLength(), "gb2312");
                        Log.i(TAG,receviceStr);

                        //Message 传递参数
                        Bundle bundle = new Bundle();
                        bundle.putString(RECV_MSG_KEY, receviceStr);
                        Message progressBarProcessMsg = new Message();
                        progressBarProcessMsg.setData(bundle);
                        progressBarProcessMsg.what = 2;
                        mHandler.sendMessage(progressBarProcessMsg);



//                        ToastUtils.show(TestActivity.this, receviceStr);
//                       showUIMsg(receviceStr);
                    }
                } catch (Exception e) {
//                    showUIMsg("报错了");
                    e.printStackTrace();
                    if (receviceSocket != null)
                    {
                        receviceSocket.close();
                    }
                                ToastUtils.show(TestActivity.this, "异常了");

                } finally {
                    if (receviceSocket != null)
                    {
                        receviceSocket.close();
                    }
                                ToastUtils.show(TestActivity.this, "异常了 finally");

                }
            }
        }).start();
    }

    /**
     * 获取当前移动设备的广播地址
     * 返回示例：192.168.11.255
     */
    private String getPhoneBroadcast() throws SocketException {
        System.setProperty("java.net.preferIPv4Stack", "true");
        for (Enumeration<NetworkInterface> niEnum = NetworkInterface.getNetworkInterfaces(); niEnum.hasMoreElements(); ) {
            NetworkInterface ni = niEnum.nextElement();
            if (!ni.isLoopback()) {
                for (InterfaceAddress interfaceAddress : ni.getInterfaceAddresses()) {
                    if (interfaceAddress.getBroadcast() != null) {
                        return interfaceAddress.getBroadcast().toString().substring(1);
                    }
                }
            }
        }
        return null;
    }






    @Override
    protected void onDestroy() {
        super.onDestroy();
//        receiveSocket.close();
//        isRunning = false;
//        System.out.println("UDP Client程序退出,关掉socket,停止广播");
//        finish();
    }

}