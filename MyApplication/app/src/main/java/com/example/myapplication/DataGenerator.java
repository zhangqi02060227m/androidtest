package com.example.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.example.myapplication.tabbar.FirstTabBarFragment;
import com.example.myapplication.tabbar.FourTabBarFragment;
import com.example.myapplication.tabbar.SecondTabBarFragment;
import com.example.myapplication.tabbar.ThreeTabBarFragment;

public class DataGenerator {

    public static final int []mTabRes = new int[]{R.drawable.tabbar_home_nor,R.drawable.tabbar_home_nor,R.drawable.tabbar_subscription_nor,R.drawable.tabbar_mine_nor};
    public static final int []mTabResPressed = new int[]{R.drawable.tabbar_home_sel,R.drawable.tabbar_home_sel,R.drawable.tabbar_subscription_sel,R.drawable.tabbar_mine_sel};

    public static final String []mTabTitle = new String[]{"首页","发现","关注","我的"};

    public static Fragment[] getFragments(String from){
        Fragment fragments[] = new Fragment[4];
        fragments[0] = FirstTabBarFragment.newInstance(from, "");
        fragments[1] = SecondTabBarFragment.newInstance(from, "");
        fragments[2] = ThreeTabBarFragment.newInstance(from, "");
        fragments[3] = FourTabBarFragment.newInstance(from, "");
        return fragments;
    }

    /**
     * 获取Tab 显示的内容
     * @param context
     * @param position
     * @return
     */
    public static View getTabView(Context context, int position){
        View view = LayoutInflater.from(context).inflate(R.layout.home_tab_content,null);
        ImageView tabIcon = (ImageView) view.findViewById(R.id.tab_content_image);
        tabIcon.setImageResource(DataGenerator.mTabRes[position]);
        TextView tabText = (TextView) view.findViewById(R.id.tab_content_text);
        tabText.setText(mTabTitle[position]);
        return view;
    }
}