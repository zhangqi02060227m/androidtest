package com.example.myapplication;

import androidx.activity.result.contract.ActivityResultContract;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.utils.SizeUtils;
import com.example.myapplication.utils.ToastUtils;
import com.google.gson.Gson;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    private TextView name_tv;
    private Button sure_btn;
    private Button next_btn, bottomNavi;
    private Button progressBtn, timerBtn, custom_dialog_btn, popBtn, fileBtn, gsonBtn, naviBtn;

    private ImageView backImg;
    private Button rv_1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);


        name_tv = findViewById(R.id.name_tv);
        sure_btn = findViewById(R.id.sure_btn);
        next_btn = findViewById(R.id.next_btn);
        progressBtn = findViewById(R.id.progress_btn);
        timerBtn = findViewById(R.id.timer_btn);
        custom_dialog_btn = findViewById(R.id.custom_dialog_btn);
//        backImg = findViewById(R.id.back_btn);
//        backImg.setVisibility(View.INVISIBLE);
        rv_1 = findViewById(R.id.recycleView_1);
        popBtn = findViewById(R.id.popUpWindow_1);
        fileBtn = findViewById(R.id.file_1);
        gsonBtn = findViewById(R.id.json_1);
        naviBtn = findViewById(R.id.navi_1);
        bottomNavi = findViewById(R.id.bottom_navi);



        LinearLayout layout = findViewById(R.id.v_layout);

        // 动态改变view带大小
        View v = findViewById(R.id.dt_v);
        ViewGroup.LayoutParams params = v.getLayoutParams();
        params.height = 50;
        params.width = 50;
        v.setLayoutParams(params);

        for (int i = 0; i < 20; i++) {
            // 动态添加imgView
            ImageView imageView = new ImageView(this);
            imageView.setImageResource(R.drawable.ic_launcher_background);
            layout.addView(imageView);
            imageView.setPadding(20,0,0,0);

            LinearLayout.LayoutParams imgParams = (LinearLayout.LayoutParams)imageView.getLayoutParams();
            imgParams.width = 100;
            imgParams.height = 100;
            imageView.setLayoutParams(imgParams);
        }



        layout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 200));



        bottomNavi.setHeight(100);
//        initView();

        // 第一种写法
//          sure_btn.setOnClickListener(new View.OnClickListener() {
//              @Override
//              public void onClick(View view) {
//              }
//          });

        // 第二种写法
        sure_btn.setOnClickListener(clickEvent);
        next_btn.setOnClickListener(clickEvent);
        progressBtn.setOnClickListener(clickEvent);
        timerBtn.setOnClickListener(clickEvent);
        custom_dialog_btn.setOnClickListener(clickEvent);
        findViewById(R.id.addView_btn).setOnClickListener(clickEvent);
        findViewById(R.id.fragment_btn).setOnClickListener(clickEvent);
        rv_1.setOnClickListener(clickEvent);
        popBtn.setOnClickListener(clickEvent);
        fileBtn.setOnClickListener(clickEvent);
        gsonBtn.setOnClickListener(clickEvent);
        naviBtn.setOnClickListener(clickEvent);
        bottomNavi.setOnClickListener(clickEvent);
        Log.i(this.getLocalClassName(), Thread.currentThread().getName());


        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.i(getLocalClassName() + "1", Thread.currentThread().getName());

            }
        });

//        new Thread("thread_name") {
//            int count = 1;
//            @Override
//            public void run() {
//                super.run();
//                while (true) {
//                    Log.i("1", String.valueOf(count++));
//                }
//            }
//
//        }.start();

    }



    private Handler handler = new Handler(Looper.myLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            if (msg.what == 1) {
                Log.i("1", "myThread start");

                sure_btn.setText(msg.obj.toString());
            } else if (msg.what == handle_time_id) {
                int value = (int) msg.obj;
                timerBtn.setText(String.valueOf(value));

            }

        }
    };

    private Thread myThread = new Thread(new Runnable() {
        @Override
        public void run() {
            Message msg = Message.obtain();
            msg.obj = "1";
            handler.sendMessage(msg);

        }
    });

    private int max_time = 100;
    private int start_time = 0;

    private int handle_time_id = 10;

    private void startTimer() {
        Log.i("startTimer", String.valueOf(start_time));

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
//                    Log.i("startTimer", String.valueOf(start_time));
                if (start_time < max_time) {

                    start_time++;
                    Message msg = new Message();
                    msg.what = handle_time_id;
                    msg.obj = start_time;
                    handler.sendMessage(msg);
                }

            }
        }, 0, 1000);
    }

    // 定时器
    private Handler handlerTimer = new Handler();


    private View.OnClickListener clickEvent = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.sure_btn: {
                    Log.i(getLocalClassName(), sure_btn.getText().toString());

                }
                break;
                case R.id.next_btn: {
//                    Toast.makeText(MainActivity.this, "11", Toast.LENGTH_SHORT).show();
//                    Log.i("打印日志Tag", next_btn.getText().toString());
//
//
                    Intent intent = new Intent(MainActivity.this, TestActivity.class);
                    startActivity(intent);
//                    intent.putExtra("name", "zhang qi");
//                    startActivityForResult(intent, 100);

                }
                break;
                case R.id.progress_btn: {
                    ProgressDialog dialog = new ProgressDialog(MainActivity.this);
                    dialog.setMessage("我们正在拼命的加载中");
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.show();

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Log.i("111   ", Thread.currentThread().toString());
                            dialog.dismiss();
                        }
                    }, 3000);


//                TimerTask task = new TimerTask() {
//                    @Override
//                    public void run() {
//                        Log.i("111   ", Thread.currentThread().toString());
//                        dialog.cancel();
//                    }
//                };
//                Timer timer = new Timer();
//                timer.schedule(task, 3000);
                }
                break;
                case R.id.timer_btn: {
                    startTimer();

                }
                break;
                case R.id.custom_dialog_btn: {
                    CustomDialog dialog = new CustomDialog(MainActivity.this);
                    dialog.setTitle("自定义标题").setMessage("message").setCancel("cancel", new CustomDialog.OnCancelInterface() {
                        @Override
                        public void cancel() {

                        }
                    }).setConfirm("ok", new CustomDialog.OnConfirmInterface() {
                        @Override
                        public void confirm() {

                        }
                    }).show();

                }
                break;
                case R.id.addView_btn: {
                    Toast.makeText(MainActivity.this, "点击了", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(MainActivity.this, addViewActivity.class);
                    intent.putExtra("name", "张琦");
                    intent.putExtra("age",22);
                    startActivity(intent);
                } break;
                case R.id.fragment_btn: {
//                    timerBtn.setVisibility(View.GONE);
                    Intent intent = new Intent(MainActivity.this, FragmentActivity.class);
                    startActivity(intent);
                } break;
                case R.id.recycleView_1:{
                    Intent intent = new Intent(MainActivity.this, RecycleView1Activity.class);
                    startActivity(intent);
                } break;
                case R.id.popUpWindow_1: {
                    ArrayList<String> list = new ArrayList<>();
                    for (int i = 0; i < 10; i++) {
                        list.add(String.valueOf(i));
                    }
                    Log.i("11","   点击了");
                    PopUpListWindow pop = new PopUpListWindow(MainActivity.this, "标题", list);
                    pop.setWidth((int) (SizeUtils.getScreenWidth(MainActivity.this) * 0.75));
                    pop.setHeight((int) (SizeUtils.getScreenHeight(MainActivity.this) * 0.75));
                    pop.setFocusable(true);
                    pop.setOutsideTouchable(true);
                    pop.showAtLocation(MainActivity.this.getWindow().getDecorView(), Gravity.CENTER, 0, 0);
                    pop.adapter.setItemOnListenerInterface(new PopUpAdapter.ItemOnListenerInterface() {
                        @Override
                        public void onClickItem(int position) {
                            Log.i(getLocalClassName().toString(), "   " +  String.valueOf(position));
                        }
                    });
                    pop.setClickSureInferFace(new PopUpListWindow.ClickSureInferFace() {
                        @Override
                        public void clickOK() {
                            Log.i(getLocalClassName().toString(), "   " +  "点击了确认按钮");

                        }
                    });

//                    WindowManager.LayoutParams windowLP = MainActivity.this.getWindow().getAttributes();
//                    windowLP.alpha = 0.5f;
//                    MainActivity.this.getWindow().setAttributes(windowLP);
                } break;
                case R.id.file_1: {
                    writeFile();
                    ToastUtils.show(MainActivity.this,"我是分散老地方那是东方闪电你发啦手打");

                } break;
                case R.id.json_1: {
                    ToastUtils.show(MainActivity.this,"自定义 toastUtils");

                    // model 转 json
                    Gson gson = new Gson();
                    UserModel model = new UserModel();
                    UserModel.DataDTO dataDTO = new UserModel.DataDTO();
                    dataDTO.setNextPage(100);
                    model.setData(dataDTO);
                    model.setCode("100");
                    model.setMessage("message");
                    String jsonString = gson.toJson(model);
                    Log.i("TAG", "onClick: " + jsonString);

                    UserModel newModel = gson.fromJson(jsonString, UserModel.class);
                    Log.i("TAG", "onClick: " + newModel.getMessage());
                } break;
                case R.id.navi_1:{
                    Intent intent = new Intent(MainActivity.this, NaviActivity.class);
                    startActivity(intent);
                } break;
                case R.id.bottom_navi: {
                    Intent intent = new Intent(MainActivity.this, TabBarActivity.class);
                    startActivity(intent);
                } break;
            }

        }




    };

    public void writeFile() {
        //文件名
        String filename = "sccFile";
        //写入内容(shuaici)

        String fileContents = "这是一个要写入的内容";
        //内容不能为空
        if (fileContents.isEmpty()) {
            Log.e("File", "FileContents.isEmpty()");
            return;
        }
        BufferedWriter writer = null;
        try {
            //获取FileOutputStream对象
            FileOutputStream fos = openFileOutput(filename, Context.MODE_PRIVATE);
            //通过OutputStreamWriter构建出一个BufferedWriter对象
            writer = new BufferedWriter(new OutputStreamWriter(fos));
            //通过BufferedWriter对象将文本内容写入到文件中
            writer.write(fileContents);

        } catch (IOException e) {
            Log.e("File", e.getMessage());
        } finally {
            try {
                //不管是否抛异常，都手动关闭输入流
                if (writer != null) writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100) {
            int age = data.getIntExtra("age", 0);
            Log.i("Tag", "onActivityResult: " + String.valueOf(age));
        }
    }

    /**
     * @author juice
     * @date 2021/12/16
     * @desc 适配 activityResult
     */
    public abstract class ResultContract extends ActivityResultContract<Bundle, Intent> {

        @NonNull
        @Override
        public Intent createIntent(@NonNull Context context, Bundle bundle) {

            return getIntent(bundle);
        }

        @Override
        public Intent parseResult(int i, @Nullable Intent intent) {
            return intent;
        }

        protected abstract Intent getIntent(Bundle bundle);
    }



    void initView() {
        findViewById(R.id.next_btn).setOnClickListener(view -> {
            Seller s1 = new Seller("张三");
            Seller s2 = new Seller("李四");
            Seller s3 = new Seller("王五");

            s1.start();
            s2.start();
            s3.start();
        });
    }

    private static Integer ticket = 10;
    private StringBuilder stringBuilder = new StringBuilder();

    void setText(String text) {
        stringBuilder.append(text).append("\n");
        name_tv.setText(stringBuilder.toString());
    }


    public class Seller extends Thread {


        private String sellername;
        private final Object lock = new Object();

        public Seller(String sellername) {
            super();
            this.sellername = sellername;
        }

        @Override
        public void run() {
            while (ticket > 0) {
                try {
                    Thread.sleep(300);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                synchronized (lock) {
                    if (ticket > 0) {
                        ticket--;
                        final int ticketNum = ticket;
                        MainActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                setText(sellername + "卖出一张票，还剩下" + ticketNum + "张");

                            }
                        });

                    }
                }
            }
        }

    }


}

