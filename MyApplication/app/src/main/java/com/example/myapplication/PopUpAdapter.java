package com.example.myapplication;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class PopUpAdapter extends RecyclerView.Adapter <PopUpAdapter.ViewHolder> {

    private Activity content;

    private ArrayList<String> list;

    private ItemOnListenerInterface itemOnListenerInterface;

    public void setItemOnListenerInterface(ItemOnListenerInterface itemOnListenerInterface) {
        this.itemOnListenerInterface = itemOnListenerInterface;
    }



    public PopUpAdapter(Activity content, ArrayList<String> list) {
        this.content = content;
        this.list = list;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_popupwindowlist_item,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textView.setText(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView textView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.title_1);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemOnListenerInterface != null) {
                        itemOnListenerInterface.onClickItem(getAdapterPosition());
                    }
                }
            });
        }

    }

    public interface ItemOnListenerInterface {
        void onClickItem(int position);
    }
}
