package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.myapplication.fragment.AFragment;
import com.example.myapplication.fragment.BFragment;

public class FragmentActivity extends AppCompatActivity {

    private Button changeBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);
        changeBtn = findViewById(R.id.change_btn);

        AFragment aFragment = new AFragment();
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.fragment_1, aFragment);
        transaction.commitAllowingStateLoss();;


        changeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager manager = getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                BFragment bFragment = new BFragment();
                transaction.replace(R.id.fragment_1, bFragment);
                transaction.commitAllowingStateLoss();;

            }
        });
    }
}