package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.os.Bundle;
import android.widget.HorizontalScrollView;

import java.util.ArrayList;

public class RecycleView1Activity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ArrayList<String> list = new ArrayList<String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycle_view1);
        recyclerView = findViewById(R.id.recycleView_id);

        for (int i = 0; i < 10; i++) {
            list.add("奥尔良鸡腿"+i+"，美味鲜香，快来试试！便宜实惠又健康，还在等待什么呢！！！！");
        }

        RecycleViewAdapter adapterDome = new RecycleViewAdapter(this,list);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.HORIZONTAL);
//        StaggeredGridLayoutManager stagger = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapterDome);

    }
}